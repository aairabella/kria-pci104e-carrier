#  Ambitious: Edge AI Platform

With the growing technological capability that allows us to measure an increasing amount of data of all kinds every day, the obvious need arises to store and process that data to convert it into useful information for decision-making. The concept of the Internet of Things (IoT) is no longer unfamiliar, and it is normal to see, understand, and realize that most of the electronic devices we use in daily life are or will soon be connected to the internet.

The generation of large amounts of data, due to electronic sensors spread everywhere, gives rise to a new discipline known as Edge Computing. This concept involves bringing processing as close as possible to the source where the data is generated.

Typically, devices that generate data are very low power and with little or no energy generation, the only available source being a battery. Therefore, if we want to bring computing closer to the data sources, it is essential that the computers used are very low power and low cost.

Moreover, given the constant change in the ways of generating and processing data, the emergence of new processing algorithms, and the growing use of increasingly optimized Machine Learning and Artificial Intelligence, there is a requirement for Edge Computing: the computing devices must be fully reconfigurable.

This work involves developing a high-capacity computing platform for one of the fastest-growing areas in recent decades, aerospace.

For the reasons mentioned above, it is necessary to select an edge processing platform for aerospace that meets the following requirements:
- Low cost.
- Low power consumption.
- High computing power.
- High flexibility for peripheral connections.
- Reprogrammability and reconfigurability.
- Easy integration with other available market elements.

A Kria-SOM platform from AMD/Xilinx is selected for this work, adapted to meet industrial and aerospace computing standards, which can be the starting point for the most varied Edge Computing solutions. The selected board standard is PC/104, in its versions 2.6 and 3.0.

The PC/104 standard is selected because it is widely used in industrial applications, but mainly in cubesats and smallsats. Figure 1 shows the proposed stack-up. On the top side, the board is compatible with the PC/104 version 2.6 standard, allowing it to connect to any standard cubesat bus system. On the bottom side, the board is compatible with PCIe/104 version 3.0, allowing it to connect with all types of industrial boards and other high-speed payloads, such as solid-state drives, sensors, among others.

![Figure 1](doc/img/HW_stackup.png)

Figure 1

Figure 2 shows a 3D render of the proposed final board.
![Figure 2](doc/img/HW_render.png)

Figure 2


Figure 3 shows a block diagram of the first version of the Ambitious Platform.

![Figure 3](doc/img/HW_Sch.png)

Figure 3